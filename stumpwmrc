;; -*-lisp-*-
;;

(in-package :stumpwm)


;; change the prefix key to something else
(set-prefix-key (kbd "C-z"))

;; Cursor
(run-shell-command "xsetroot -cursor_name left_ptr -solid black -name root-window")

;; Pulseaudio
(run-shell-command "pulseaudio --start")

;; XScreenSaver
(run-shell-command "xscreensaver -nosplash")

;; Some other basic settings
(setf *timeout-wait* 10)
(setf *message-window-gravity* :center)
(setf *input-window-gravity* :center)

;; Mouse focus switches frame:
(setf *mouse-focus-policy* :click)


;; Mark the Default group as "safe" with border color:
(set-border-color "green")
(set-msg-border-width 3)

;; Modeline behavior
;; TODO: create a custom format that shows windows like this:
;; [ 0-termi 1*emacs 2+Conker 3-stumpw 4-Home ]
;; window number + only 4-6 characters of the title
(setf *screen-mode-line-format*
      (list "[%n] %W | "
	    '(:eval (run-shell-command "date +\"%a %b %d %H:%M %Y\"" t))))

(setf *mode-line-timeout* 60)

(defun focus-group-cb (cgroup lgroup)
  "Toggle mode line only when entering Default from non-Default"
  (if (or (string= "Default" (group-name cgroup))
	  (string= "Default" (group-name lgroup)))
      (toggle-mode-line (current-screen) (current-head))))

(add-hook *focus-group-hook* 'focus-group-cb)



;;
;; Sandboxing
;;
;; I run a bunch of things wrapped in Fedora sandbox(8) containers.
;; Idea here is to isolate activities like web browsing, email, IM etc. into separate
;; sandboxes which cannot access resources from other groups. Poor man's qubes OS :)
;;
;; In the sandboxes, I also run StumpWM. By default, Xephyr locks the input with "Ctrl+shift"
;; but for single shots, I usually just hit "C-z z". I also mark input boxes and mode-line
;; with sandbox-specific colors.
;;

;; Sandbox parameters

(defparameter *res* "1600x1200")              ; Usually the same as main resolution
(defparameter *wm* "/usr/local/bin/stumpwm")  ; Window manager run *inside* sandbox
(defparameter *home* (getenv "HOME"))

(load "~/bin/bank.lisp")             ; At least (defparameter *sandbox-bank-url* "...")

;; Sandbox parameters: name, type and command to run (see sandbox(8) for more info):

(defparameter *sandbox-list* (list
			       (list "mail" "sandbox_net_client_t" "firefox")
			       (list "im" "sandbox_web_t" "pidgin")
			       (list "net" "sandbox_web_t" "firefox")
			       (list "spotify" "sandbox_web_t" "spotify")
			       (list "chrome" "sandbox_web_t" "google-chrome --no-sandbox")
			       (list "bank" "sandbox_web_t"
				     (concatenate 'string "firefox " *sandbox-bank-url*))))

;; Some notes:
;; - I have separate home -and tmp-directories for the containers, in $HOME/$sandbox_{home,tmp}
;;   (for example, /home/bob/net_home)
;; - In each $HOME/$sandbox_home, I have a copy of .stumpwmrc which looks like this:
;;     (in-package :stumpwm)
;;     (set-prefix-key (kbd "C-z"))
;;     (setf *mode-line-background-color* "red")
;;     (setf *mode-line-foreground-color* "black")
;;     (set-border-color "red")
;;     (set-msg-border-width 3)
;;     (setf *screen-mode-line-format*
;;     (list "[%n] %W | "
;;         '(:eval (run-shell-command "date +\"%a %b %d %H:%M %Y\"" t))))
;;     (mode-line)
;; - TODO: automate creation of child stumpwmrc's, define colors etc. in *sandbox-list*
;;

(defun sandbox-command (name)
  "Find named sandbox and return command string."
  (let ((sandbox (assoc name *sandbox-list* :test #'string=)))
    (if (null sandbox)
	(throw 'stumpwm::error "No such sandbox.")
	(concatenate 'string
		     "sandbox -X "
		     "-w " *res* " "
		     "-W " *wm* " "
		     "-H " *home* "/" name "_home "
		     "-T " *home* "/" name "_tmp "
		     "-t " (second sandbox) " "
		     (third sandbox)))))

;; Run named sandbox, but only if it doesn't exist (or has no windows)
(defcommand run-sandbox (name) ((:rest "Sandbox name: "))
	    (let* ((grp (gnew name))(wcount (group-windows grp)))
	      (if (null wcount)
		  (run-shell-command (sandbox-command name)))))

;; Mail
(defcommand run-mail () ()
	    (run-sandbox "mail"))

;; IM
(defcommand run-im () ()
	    (run-sandbox "im"))

;; General networking, like firefox and spotify
(defcommand run-net () ()
	    (run-sandbox "net"))

;; Chrome
(defcommand run-chrome () ()
	    (run-sandbox "chrome"))

;; Spotify
(defcommand run-spotify () ()
	    (run-sandbox "spotify"))

;; Banking
(defcommand run-bank () ()
	    (run-sandbox "bank"))

;; Floating group for bad boys like zynaddsubfx and qjackctl
(defcommand run-music () ()
	    (let* ((win (gnew-float "music"))(wcount (group-windows win)))
		   (if (null wcount)
		       (run-shell-command "gnome-terminal"))))


;; Sandbox clipboard handling

;; Since sandboxes are run in separate X-servers, they cannot access the clipboard.
;; Clipboards are controlled from host stumpwm with commands get-clip and set-clip.

(defun copy-clip (from to)
  "Copy clipboard from sandbox to another."
  (cond
    ((string-equal "Default" to)
     (run-shell-command (format nil "get_clip.sh ~a" from)))
    ((string-equal "Default" from)
     (run-shell-command (format nil "set_clip.sh ~a" to)))
    (t
     (run-shell-command (format nil "clip.sh ~a ~a" from to)))))

(defcommand get-clip (wfrom) ((:string "Where from: "))
	    (let ((from wfrom)(to (group-name (current-group))))
	      (if (string-equal from "")
		  (setf from "Default"))
	      (copy-clip from to)))

(defcommand set-clip (wto) ((:string "Where to: "))
	    (let ((to wto)(from (group-name (current-group))))
	      (if (string-equal to "")
		  (setf to "Default"))
	      (copy-clip from to)))


;; Volume control

(define-key *root-map* (kbd "C-,") "exec amixer set Master 5%-")
(define-key *root-map* (kbd "C-.") "exec amixer set Master 5%+")
(define-key *root-map* (kbd "C--") "exec amixer set Master toggle")


;; Misc. commands and key bindings

;; For browsing dev documents etc. - JavaScript disabled!
(defcommand browser () ()
	    (run-or-raise "conkeror" '(:class "Conkeror")))

(define-key *root-map* (kbd "C-o") "browser")

;; Gnome terminal has nice tabs
;; I have this in my .bashrc to set title to match with frame preferences:
;; export PROMPT_COMMAND='echo -ne "\033]0;terminal: ${PWD/$HOME/~}\007"'
(define-key *root-map* (kbd "c") "exec gnome-terminal")
(define-key *root-map* (kbd "C-c") "exec gnome-terminal")

;; Documents
(define-key *root-map* (kbd "d") "exec evince")
(define-key *root-map* (kbd "C-d") "exec evince")

;; Nautilus browser for pics, mounting USB etc.
(define-key *root-map* (kbd "t") "exec nautilus")
(define-key *root-map* (kbd "C-t") "exec nautilus")


;; Default group configuration

(defun create-dev-group ()
  "Restore frame settings and run some programs on the Default group."

  ;; "default.dump" contains a simple frame setting saved with "dump-group-to-file"
  ;; frame #0: left upper frame
  ;; frame #1: right (about half of the screen)
  ;; frame #2: left lower frame (about 2/3 of the height)
  (restore-from-file "default.dump")
  (restore-window-placement-rules "default.win")

  ;; I like to place terminal to the frame #0, emacs to #1 and browser to #2:
  ;; XXX: what's the use of this, after restore-window-placement-rules?
  ;; This alone doesn't seem to do the trick, so placement rules are needed...
  (define-frame-preference "Default"
    (0 t t :title "terminal")
    (1 t t :title "emacs")
    (2 t t :class "Conkeror"))

  ;; Run them and hope they end up to right frames:
  (run-commands "exec gnome-terminal"
		"emacs"
		"browser"))

(defcommand rundev () ()
	    (create-dev-group))


;; Finally, just run some programs on the Default group:
(rundev)

