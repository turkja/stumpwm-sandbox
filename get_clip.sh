#!/bin/sh

# Get clipboard from container

if [ ! -e ~/$1_home/seremote ]; then
    echo "$1: no such guest"
    exit 1
fi

screen=`grep DISPLAY ~/$1_home/seremote | sed 's/DISPLAY=\(:.\) .*/\1/g'`

xsel --display $screen -b -o | xsel -b -i
