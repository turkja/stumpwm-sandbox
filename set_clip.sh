#!/bin/sh

# Set clipboard of container and clear it after timeout
# Timeout 0 means no clearing
# NOTE: clearing is very important in case of passwords!!

if [ ! -e ~/$1_home/seremote ]; then
    echo "$1: no such guest"
    exit 1
fi

screen=`grep DISPLAY ~/$1_home/seremote | sed 's/DISPLAY=\(:.\) .*/\1/g'`

# Set clipboard
xsel -b -o | xsel --display $screen -b -i

# Clear clipboard (default=10s)
timeout=30
if [ ! -z "$2" ]; then
    timeout=$2
fi
if [ $timeout -ne 0 ]; then
    sleep $timeout
    echo -n | xsel --display $screen -b -i
fi


