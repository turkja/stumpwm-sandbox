#!/bin/sh

SEC=`ls -Z ~/$1_home/seremote | sed 's/.*s0:\(.*\) .*/\1/g'`

ps -exwwZ | grep $SEC | awk '{print $2,$3,$4,$5,$6}' | grep -v grep

