
# StumpWM configuration

Running SELinux containers from [StumpWM](https://stumpwm.github.io/) - aka the poor man's Qubes OS :)

StumpWM is awesome. It gets the job done and it doesn't bother you with sounds, visual alerts,
bells and whistles and fairies. I've found it very useful in integrating with my SELinux
container setup.

Basically, I just run a buch of containers for various activities, such as web browsing, email,
online banking and so on. Code running inside these containers cannot access other containers, so
I can run ghetto bytes like the chrome or spotify. But unlike running full VM images, these containers
really run with full native speed so using them is actually practical.

StumpWM init file (stumpwmrc) should be quite clear and I also wrote few supporting scripts
for manipulating container clipboards. There are still some things left to do, like automating the
guest StumpWM setups (yes, I also run it on the guests as nested WM's) and cleaning/resetting
containers. That I still do manually.


## So why all this trouble? Why not just use Qubes?

Qubes is complicated software developed by a relatively small community. There's no doubt - Qubes
is much more secure than my SELinux setup - but practically speaking, the container approach works
very well for my purposes and is very secure.

If something crawls in from for example JavaScript running on the firefox in "net" container, it
can infect the container configuration files, but it cannot read data from the host system or
other containers. Then there is still the relatively large attack surface of the Linux kernel,
since the containers run on a single kernel, but that's a risk I'm taking.

Containers are very small and simple to monitor, only few processes running. And they are
very cheap to wipe away, just remove everything the home directory. Then you lose application
configurations, but that can be recreated etc.

Added bonus is that all software gets updated always with the host OS (to be fair, Qubes also does
takes care of that).

All in all, I've found the container approach perfect balance of security, usability and performance.
This is very important because security that is not usable is not security. I've tried lots of things
over the years, like separate VM's, separate machines etc. I feel that the containers are going to stay.

If I need to take it to next level, I start running offline desktop computer in radio-shielded room...


## How to transfer data in/out of containers

Host StumpWM has full access to containers, so you can just copy data from the container home
directory and use the supplied helper functions for moving clipboard in and out (see set-clip and
get-clip).

This is the only thing that gets in my way, but I kinda like it because it forces me to *think* what
sort of data I want to move in and out of containers and how. For example, I always run keepassx on
the host StumpWM and then use set-clip for sending password to container. I never store passwords in
the container.

## Other issues

This simple container-based setup is far from perfect. For example, on typical Linux
distributions, the audio configuration with pulse and alsa is a mess. If something hits
the container, it can probably start recording the microphone without some extra
tweaking. If possible, microphone should be disabled from BIOS anyways.

StumpWM takes care of other physical security issues, like external USB
devices naturally, because it has no code handling that automatically :)

