#!/bin/sh

# Backup current clipboard
xsel -b -o > /tmp/.xsel.current

# Get clipboard of source container
get_clip.sh $1

# Set clipboard of target container with timeout 0 -> no clear
set_clip.sh $2 0

# Restore current clipboard
cat /tmp/.xsel.current | xsel -b -i

